/*
 * Copyright (c) 2018, SyedW
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 * File:   DH_ThreadedObject.hpp
 * Author: phytress
 *
 * Created on July 9, 2017, 1:22 PM
 */

#ifndef DH_THREADLOCKEDOBJECT_HPP
#define DH_THREADLOCKEDOBJECT_HPP

#include <pthread.h>

namespace DigitalHaze {

	class ThreadLockedObject {
	public:
		ThreadLockedObject() noexcept;
		~ThreadLockedObject();

		inline void LockObject() {
			pthread_mutex_lock(&cs_mutex);
		}

		inline void UnlockObject() {
			pthread_mutex_unlock(&cs_mutex);
		}

		inline void WaitOnCondition(pthread_cond_t& cond) {
			pthread_cond_wait(&cond, &cs_mutex);
		}

		inline static void SignalCondition(pthread_cond_t& cond) {
			pthread_cond_signal(&cond);
		}

		inline static void BroadcastCondition(pthread_cond_t& cond) {
			pthread_cond_broadcast(&cond);
		}
	private:
		pthread_mutex_t cs_mutex;
	};
}

#endif /* DH_THREADEDOBJECT_HPP */

