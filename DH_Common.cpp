/*
 * Copyright (c) 2018, SyedW
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#define _GNU_SOURCE

#include "DH_Common.hpp"

#include <stdarg.h>
#include <string.h>

#include "zlib.h"

std::string DigitalHaze::stringprintf(const char* fmtStr, ...) {
	char* message = nullptr;
	int msgLen;

	va_list list;
	va_start(list, fmtStr);
	msgLen = vasprintf(&message, fmtStr, list); // God bless GNU
	va_end(list);

	if(msgLen == -1) return std::string(""); // allocation error
	if(!message) return std::string(""); // I think only *BSD does this

	std::string retStr = message;

	free(message); // vasprintf requires free

	return retStr;
}

void DigitalHaze::displayformatted(void* buf, size_t len) {
	char readableBuf[17];
	unsigned char* packet = (unsigned char*) buf;

	for(size_t pos = 0; pos < len; pos += 16) {
		int i;
		memset(readableBuf, 0, sizeof( readableBuf));

		printf("%.4zX| ", pos);
		for(i = 0; i < 16 && pos + i < len; ++i) {
			readableBuf[i] = packet[ pos + i ] < 0x20 ? '.' : (char) packet[ pos + i ];
			printf("%.2hhX ", packet[ pos + i ]);
		}
		for(; i < 16; ++i) printf("   ");
		printf("|%s\n", readableBuf);
	}
}

int DigitalHaze::stringcasecmp(std::string str1, std::string str2) {
	return strcasecmp(str1.c_str(), str2.c_str());
}

std::string DigitalHaze::zliberr(int errCode) {
	switch(errCode) {
		case Z_BUF_ERROR: return "Destination buffer not large enough";
			break;
		case Z_MEM_ERROR: return "insufficient memory";
			break;
		case Z_DATA_ERROR: return "compressed data was corrupted";
			break;
	};

	return stringprintf("Unknown error code: %d", errCode);
}