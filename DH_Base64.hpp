/*
 * Copyright (c) 2018, SyedW
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 * File:   DH_Base64.hpp
 * Author: Syed Ali <Syed.Ali@digital-haze.org>
 *
 * Created on September 16, 2017, 4:22 PM
 */

#ifndef DH_BASE64_HPP
#define DH_BASE64_HPP

#include <cstdlib>

namespace DigitalHaze {
	// Converts an int (first 6 bits only) to a base64 char.
	// If the int is less than 0 or above 63, then 0x00 is returned
	char IntToBase64Char(int num);

	// Converts a single base64 char to an int
	// If the char is invalid OR a '=' (which is valid), it returns zero
	int Base64CharToInt(char c);

	// Converts the src binary data (of length len) to a Base64 string stored in dest.
	// A null terminator is stored in dest as well. Returns the length of the string
	size_t Base64Encode(void* src, void* dest, size_t srclen);

	// Converts the src base64 data (of length len) to the decoded values, stored in dest.
	// A null terminator is stored in dest as well. Returns the length of the data
	size_t Base64Decode(void* src, void* dest, size_t srclen);
}

#endif /* DH_BASE64_HPP */

