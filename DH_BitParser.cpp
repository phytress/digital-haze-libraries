/*
 * Copyright (c) 2018, SyedW
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "DH_BitParser.hpp"

namespace DigitalHaze
{

	BitParser::BitParser(void* buffer, size_t lenInBits) noexcept
	: dataPtr((unsigned char*) buffer), lengthInBits(lenInBits), startBitPos(0) {
	}

	BitParser::BitParser(const BitParser& other) noexcept
	: dataPtr(other.dataPtr), lengthInBits(other.lengthInBits), startBitPos(other.startBitPos) {
	}

	BitParser::BitParser(BitParser&& other) noexcept
	: dataPtr(other.dataPtr), lengthInBits(other.lengthInBits), startBitPos(other.startBitPos) {
	}

	BitParser::~BitParser() noexcept {
	}

	BitParser& BitParser::operator=(const BitParser& other) noexcept {
		dataPtr = other.dataPtr;
		lengthInBits = other.lengthInBits;
		startBitPos = other.startBitPos;
		return *this;
	}

	BitParser& BitParser::operator=(BitParser&& other) noexcept {
		dataPtr = other.dataPtr;
		lengthInBits = other.lengthInBits;
		startBitPos = other.startBitPos;
		return *this;
	}

	void BitParser::offsetBuffer(ptrdiff_t offsetInBits) {
		startBitPos = (ptrdiff_t) startBitPos + offsetInBits;

		if(startBitPos < 0) {
			// Move back appropriate number of bytes
			dataPtr -= 1 + ((startBitPos - 1) / -8);
			startBitPos &= 7; // Clear negative value
		} else if(startBitPos > 7) {
			// Move forward appropriate number of bytes
			dataPtr += startBitPos / 8;
			startBitPos &= 7; // Remainder after /8
		}

		lengthInBits = (ptrdiff_t) lengthInBits - offsetInBits;
	}

	SingleBitDescriptor BitParser::operator[](ptrdiff_t index) const {
		ptrdiff_t bytePos;
		int bitPos;

		if(index > 0) {
			bytePos = index / 8;
			bitPos = (int) ((index % 8) + startBitPos);
		} else if(index < 0) {
			bytePos = -1 + ((index + 1) / 8);
			bitPos = (int) ((index & 7) + startBitPos); // Remainder after /8 and clear negative
		} else // index == 0
		{
			bytePos = 0;
			bitPos = (int) startBitPos;
		}

		if(bitPos > 7) {
			bitPos -= 8;
			bytePos++;
		}

		return SingleBitDescriptor(dataPtr + bytePos, bitPos);
	}

	SingleBitDescriptor BitParser::operator*() const {
		return SingleBitDescriptor(dataPtr, (unsigned char) startBitPos);
	}

	BitParser& BitParser::operator++() {
		offsetBuffer(1);
		return *this;
	}

	SingleBitDescriptor::SingleBitDescriptor(unsigned char* bptr, unsigned char bit) noexcept
	: bytePtr(bptr), bitPos(bit) {
	}

	SingleBitDescriptor& SingleBitDescriptor::operator=(bool rhs) {
		if(rhs) *bytePtr |= 1 << bitPos;
		else *bytePtr &= ~(1 << bitPos);
		return *this;
	}

	SingleBitDescriptor& SingleBitDescriptor::operator=(int rhs) {
		*this = (bool)!!rhs;
		return *this;
	}

	SingleBitDescriptor& SingleBitDescriptor::operator=(SingleBitDescriptor& rhs) noexcept {
		*this = (bool)rhs;
	}

	bool SingleBitDescriptor::operator==(bool rhs) const {
		return(bool) * this == rhs;
	}

	bool SingleBitDescriptor::operator!=(bool rhs) const {
		return !(*this == rhs);
	}

	SingleBitDescriptor::operator int() const {
		return !!(*bytePtr & (1 << bitPos));
	}

	SingleBitDescriptor::operator bool() const {
		return !!(*bytePtr & (1 << bitPos));
	}

	BitBufferIterator BitParser::begin() const {
		return BitBufferIterator(this, 0);
	}

	BitBufferIterator BitParser::end() const {
		return BitBufferIterator(this, lengthInBits);
	}

	BitBufferIterator::BitBufferIterator(const BitParser* bptr, size_t bpos) noexcept
	: bbPtr(bptr), pos(bpos) {
	}

	bool BitBufferIterator::operator!=(BitBufferIterator& rhs) const {
		return pos != rhs.pos;
	}

	SingleBitDescriptor BitBufferIterator::operator*() const {
		return(*bbPtr)[pos];
	}

	BitBufferIterator& BitBufferIterator::operator++() {
		pos++;
		return *this;
	}

}