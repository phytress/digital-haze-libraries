/*
 * Copyright (c) 2018, SyedW
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 * File:   DH_TCPClientSocket.hpp
 * Author: phytress
 *
 * Created on July 9, 2017, 12:59 AM
 */

#ifndef DH_TCPCLIENTSOCKET_HPP
#define DH_TCPCLIENTSOCKET_HPP

#include "DH_TCPSocket.hpp"
#include "DH_ThreadLockedObject.hpp"

#include <signal.h>
#include <pthread.h>

#include <sys/socket.h>
#include <sys/types.h>

#include <netdb.h>

namespace DigitalHaze {

	class TCPClientSocket : public TCPSocket, public ThreadLockedObject {
	public:
		TCPClientSocket();
		virtual ~TCPClientSocket();

		// A blocking connect attempt. Will return true if successful.
		// False if connection did not go through.
		bool AttemptConnect(const char* hostname, unsigned short port);

		// A non-blocking connect attempt.
		// The return is true if everything is going fine (not if you're
		// connected). If it is false, the thread or some other part
		// failed.
		bool AttemptThreadedConnect(const char* hostname, unsigned short port);

		// If a threaded attempt to connect was attempted
		// this function will let you know if the thread
		// is still in process of connecting.
		bool isConnecting() const;

		// Returns what address is currently being used, whether the
		// connection is still connecting or already connected.
		void GetConnectedAddress(sockaddr* addrOut) const;
		void GetConnectedAddressLen(socklen_t& addrLen) const;

		// Override of closing a TCP socket. Zero more data from this class.
		virtual void CloseSocket() override;

		// Our thread needs access to our internals
		friend void* ConnectThread(void*);
	private:
		// Thread status
		volatile sig_atomic_t connectThreadStatus;
		// Thread pointer
		pthread_t connectThread;

		// Resolved connected address
		TCPAddressStorage connectedAddress;
		socklen_t connectedAddressLen;

		void CloseCurrentThreads();
		void SetConnectedAddress(sockaddr* cAddress, socklen_t cAddressLen);

		// Give our thread access to private parent functions

		inline void Thread_RecordErrno() {
			Socket::RecordErrno();
		}

		// When there's a lot of system calls going on, thread might
		// need to cache errors and set them in our object later.

		inline void Thread_RecordErrno(int setErrno) {
			Socket::RecordErrno(setErrno);
		}

		inline void Thread_SetSockFD(int newfd) {
			Socket::sockfd = newfd;
		}
	};

	void* ConnectThread(void*);
}

#endif /* DH_TCPCLIENTSOCKET_HPP */

