/*
 * Copyright (c) 2018, SyedW
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 * File:   DH_Common.hpp
 * Author: Syed Ali <Syed.Ali@digital-haze.org>
 *
 * Created on August 15, 2017, 10:52 AM
 */

#ifndef DH_COMMON_HPP
#define DH_COMMON_HPP

#define COLOR_RED  "\x1b[31m"
#define COLOR_GREEN  "\x1b[32m"
#define COLOR_YELLOW "\x1b[33m"
#define COLOR_BLUE  "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN  "\x1b[36m"
#define COLOR_RESET  "\x1b[0m"

#include <string>

namespace DigitalHaze {
	// Converts a formatted string and parameters into a std::string
	std::string stringprintf(const char* fmtStr, ...);

	// Formats raw data for output in hex and makes it easier to read.
	void displayformatted(void* buf, size_t len);

	// Compares to strings case insensitive
	int stringcasecmp(std::string str1, std::string str2);

	// Returns a text representation of a zlib error code
	std::string zliberr(int errCode);
}

#endif /* DH_COMMON_HPP */

