/*
 * Copyright (c) 2018, SyedW
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "DH_TCPSocket.hpp"

#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <errno.h>

DigitalHaze::TCPSocket::TCPSocket() : IOSocket() {
}

DigitalHaze::TCPSocket::TCPSocket(int connectedfd) : IOSocket() {
	IOSocket::sockfd = connectedfd;
}

DigitalHaze::TCPSocket::~TCPSocket() {
}

bool DigitalHaze::TCPSocket::PerformSocketRead(size_t len, bool flush) {
	// Check how many bytes to read.
	// If not specified, then read as many as we can!
	if(!len) {
		len = IOSocket::readBuffer.GetRemainingBufferLength();
		// If we have no more space left, then read how much we'll allocate
		if(!len) {
			len = IOSocket::readBuffer.GetBufferReallocSize();

			// Expand by our realloc size. If realloclen ends up being zero,
			// this will throw an error.
			IOSocket::readBuffer.ExpandBuffer();
		}
	}

	// Make sure we have enough space.
	if(len > IOSocket::readBuffer.GetRemainingBufferLength()) {
		// We don't have enough space, expand
		IOSocket::readBuffer
			.ExpandBuffer(len - IOSocket::readBuffer.GetRemainingBufferLength());
	}

	// read
	ssize_t nBytes = recv(Socket::sockfd,
						readBuffer.GetBufferEnd(), len,
						flush ? MSG_WAITALL : MSG_DONTWAIT);

	// error?
	if(nBytes <= 0) {
		Socket::RecordErrno();
		if(!flush && (Socket::lasterrno == EAGAIN ||
					Socket::lasterrno == EWOULDBLOCK)) {
			// If we're not flushing, then these errors are okay.
			// We just accomplished nothing instead.
			//return true;
		}
		return false;
	}

	// If we're flushing, we better have gotten everything we wanted
	if(flush && (size_t) nBytes != len) {
		Socket::RecordErrno();
		return false;
	}

	// Read successful
	IOSocket::readBuffer.NotifyWrite((size_t) nBytes);

	return true;
}

bool DigitalHaze::TCPSocket::PerformSocketWrite(bool flush) {
	// Can't write to the socket if we have no data
	if(!IOSocket::writeBuffer.GetBufferDataLen()) return false;

	size_t totalWritten = 0;

	do {
		// Attempt send
		ssize_t nBytes = send(
							IOSocket::sockfd,
							(void*) ((size_t) writeBuffer.GetBufferStart() + totalWritten),
							IOSocket::writeBuffer.GetBufferDataLen() - totalWritten,
							flush ? 0 : MSG_DONTWAIT);

		if(nBytes <= 0) {
			Socket::RecordErrno();
			return false;
		}

		totalWritten += (size_t) nBytes;

		// Repeat until fully sent if flushing
	} while(flush && totalWritten < IOSocket::writeBuffer.GetBufferDataLen());

	// Remove the data we just wrote.
	IOSocket::writeBuffer.ShiftBufferFromFront(totalWritten);

	return true;
}

bool DigitalHaze::TCPSocket::isConnected() const {
	return IOSocket::sockfd != -1;
}

bool DigitalHaze::TCPSocket::ConvertAddrToText(TCPAddressStorage& addr,
											   socklen_t len,
											   char* outText) {
	void* paddr;

	switch(addr.sa.sa_family) {
		case AF_INET:
			paddr = &addr.sa_ip4.sin_addr;
			break;
		case AF_INET6:
			paddr = &addr.sa_ip6.sin6_addr;
			break;
		default: // Don't know what kind of address this is.
			return false;
	};

	return nullptr != inet_ntop(addr.sa.sa_family, paddr, outText, len);
}